<?php

namespace Gravility\Http\Controllers;

use Illuminate\Http\Request;
use Gravility\Logic\CubeSummation;

class P1Controller extends Controller
{

    /**
    * Cube Summation: explicacion del problema.
    * Comenzar: nos permite realizar nuestros test_case.
    * @return Response 
    **/
    public function index(){
        return view('p1.home');
    }

    public function store(Request $request){
      $cube = new CubeSummation;
      $respuestas = $cube->summation($request);
      return response()->json( $respuestas);
    }

    /**
    * Creación de casos de prueba.
    * 
    * @return View 
    **/
    public function create(){
    	return view('p1.create');
    }

    /**
      * Lista los cubos ingresados previamente en el sistema. 
      *
      * @return View
      **/

    public function show($id){

    }


}
