<?php


namespace Gravility\Logic;

use Illuminate\Http\Request;

class CubeSummation
{ 


  public function summation(Request $request){
      
      //Este arreglo contendrá las operaciones que se van a hacer en el sistema.
      //el primer elemento identifica el caso de prueba, 
      //el segundo elemento identifica la operacion a realizar,
      //el tercer elemento identifica para una operacion de consulta el valor incial desde donde comienza la sumatoria,
      //el cuarto elemento identifica para una operacion de consulta el valor final donde termina la suma,
      //el tercer elemento identifica para una operacion de actualizacion la posicion de actualizacion,
      //el cuarto elemento identifica para una operacion de actualizacion el valor a ingresar.
      $operaciones = array();

      $operaciones = $this->getData($request);
      $respuestas = $this->result($operaciones);

      return $respuestas;
  }

  public function getData(Request $request){
    //Codigo para recuperar los valores del formulario

      //Se toman el numero de casos de prueba dados por el usuario
      $test_cases = $request->input('test_cases');  

      //Esta variable contendrá el nro de operaciones por caso de prueba de operaciones.
      $nro_operaciones = 0;


      //Este arreglo contendrá las operaciones que se van a hacer en el sistema.
      //el primer elemento identifica el caso de prueba, 
      //el segundo elemento identifica la operacion a realizar,
      //el tercer elemento identifica para una operacion de consulta el valor incial desde donde comienza la sumatoria,
      //el cuarto elemento identifica para una operacion de consulta el valor final donde termina la suma,
      //el tercer elemento identifica para una operacion de actualizacion la posicion de actualizacion,
      //el cuarto elemento identifica para una operacion de actualizacion el valor a ingresar.
      $operaciones = [];


      for($i=0; $i < $test_cases ; $i ++){
        //Representa el identificador para los inputs
        $id1 = $i+1;

        //Devuelve el tamaño de la matrix, se desprecia ya que se decidio por eficiencia de uso de recursos,
        // que no es necesario crear una matrix por cada caso de prueba.
        $tamano_matrix = $request->input('tamano_matrix_'.$id1);

        //Se toma el numero de operaciones ingresadas por el usuario.
        $nro_operaciones = $request->input('nro_operaciones_'.$id1);

        //Por cada una de esas operaciones se va a evaluar que operacion es y cuales son sus valores.
        for($h = 0; $h < $nro_operaciones ; $h ++){
          //este identificador nos permite acceder ael valor correcto dentro de los inputs del usuario.
          $id2 = $h+1;

          //Se toma la operacion para el caso de prueba id1 y la para la operación numero id2 
          $operacion = $request->input('operation_'.$id1.$id2);

          //Mira a cual es la operación que tenemos que guardar
          if($operacion == 'U'){            
            $x = $request->input('x'.$id1.$id2);
            $valor = $request->input('valor'.$id1.$id2);
            array_push($operaciones, [$i, 'U', $x, $valor] );
          }else{
            $x1 = $request->input('x1'.$id1.$id2);
            $x2 = $request->input('x2'.$id1.$id2);
            array_push($operaciones, [$i, 'Q', $x1, $x2] );
          }
          
        }        
      }

      return $operaciones;
  }

  public function result($operaciones){
    
      //Este arreglo contendrá, las respuestas que arroja el sistema.
      $respuestas = [];
      //Este arreglo representa los valores que tendrá la matrix.
      //Inicializa todos los valores de la matrix a 0.
      $valores = $this->inicializarMatrix();

      //Esta variable determinará si entramos a evaluar otro cubo.
      $temp =0;

      //Proceso para el calculo de las respuestas.
      $max = sizeof($operaciones);

      for($i=0; $i < $max ; $i ++){
        //Esta operación nos indica cuando debemos reiniciar nuestra matrix,
        //es decir cuando entramos a otro caso de prueba. Todos los valores serán 0.
        if($temp != $operaciones[$i][0]){          
          $valores = $this->inicializarMatrix();          
        }

        //Esta condición nos dice que operación realizar.
        if($operaciones[$i][1] == 'Q'){
          //Si la operación es consultar, entonces crea un valortemp que contendrá la suma,
          //de los cubos que estén entre el rango espesificado en la consulta.
          $valorTemp = 0;

          //Se hace un for desde el primer valor de la consulta, hasta el siguiente valor.
          for($j= $operaciones[$i][2]; $j <= $operaciones[$i][3]; $j++ ){
            //Se suman todos los valor que estén entre los dos valores de la consulta
            $valorTemp +=  $valores[$j];
          }
          //Ya que es el valor de las consulta lo que nos importa, entonces lo guardamos en los resultados. 
          array_push($respuestas, [$valorTemp]);
        }else{
          //Esto guarda en la posicion el valor correspondiente a una operacion de actualización.
          $valores[$operaciones[$i][2]]= $operaciones[$i][3];
        }
        //Aquí se cambia la variable que dice si estamos evaluando un nuevo caso de prueba.
        $temp = $operaciones[$i][0];
      }

      /*
        Comentarios: Ya que no se espesifica en el problema si los valores de X, Y, y Z 
        pueden ser diferentes, entonces se toma como una regla que tanto X, como Y, como Z,
        serán el mismo valor, por lo cual se toma sólo uno de ellos.
      */
        $collection = collect($respuestas);
        
        return $collection->collapse()->all();
  }

  public function inicializarMatrix(){
    $valores = [];

    for($i =0 ; $i <= 100; $i++){
      $valores[$i] = 0;
    }

    return $valores;
  }


}