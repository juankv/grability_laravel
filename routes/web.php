<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view("welcome");

});

Auth::routes();

Route::get('/home', 'HomeController@index');

//Ruta para mi primer punto
Route::resource('p1', 'P1Controller', ['only' => ['index', 'show', 'create', 'store']]);

Route::resource('profile', 'ProfileController', ['only' => ['index', 'show', 'create', 'store']]);
