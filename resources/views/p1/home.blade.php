@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Punto 1: Cube Summation</div>

                <div class="panel-body">
                    <p> 
                        You are given a 3-D Matrix in which each 
                        block contains 0 initially. 
                        The first block is defined by the coordinate (1,1,1) 
                        and the last block is defined by the coordinate (N,N,N). 
                        There are two types of queries. 
                    <p>
                    <pre> <strong>UPDATE x y z W </strong> </pre>
                    <p> updates the value of block (x,y,z) to W.<p>
                    <pre> <strong>QUERY x1 y1 z1 x2 y2 z2 </strong></pre>
                    <p> 
                        calculates the sum of the value of blocks 
                        whose x coordinate is between x1 and x2 (inclusive), 
                        y coordinate between y1 and y2 (inclusive) 
                        and z coordinate between z1 and z2 (inclusive).
                    </p>
                    <p> <strong> Input Format </strong> <br>
                    
                        The first line contains an integer T, 
                        the number of test-cases. T testcases follow. <br>
                        For each test case, the first line will contain 
                        two integers N and M separated by a single space.  <br>
                        N defines the N * N * N matrix. <br>
                        M defines the number of operations. <br>
                    </p>

                    <p> The next M lines will contain either </p>
                    <pre>  <strong>1. UPDATE x y z W </strong> <br>  <strong>2. QUERY  x1 y1 z1 x2 y2 z2 </strong></pre>
                    <p> <strong> Output Format </strong> <br>
                        Print the result for each QUERY. 
                    </p>

                    <pre><strong> Constrains: </strong> <br>  <strong> 1 <= T <= 50 </strong> <br>  <strong> 1 <= N <= 100 </strong> <br>  <strong> 1 <= M <= 1000 </strong> <br>  <strong> 1 <= x1 <= x2 <= N </strong> <br>  <strong> 1 <= y1 <= y2 <= N </strong> <br>  <strong> 1 <= z1 <= z2 <= N </strong> <br>  <strong> 1 <= x,y,z <= N </strong> <br>  <strong> -109 <= W <= 109 </strong> <br></pre>

                    <p> <strong> Sample Input </strong> <br> 
                        <pre> <strong>2 </strong> <br> <strong>4 5 </strong> <br> <strong>UPDATE 2 2 2 4 </strong><br> <strong>QUERY 1 1 1 3 3 3 </strong><br> <strong>UPDATE 1 1 1 23 </strong><br> <strong>QUERY 2 2 2 4 4 4 </strong> <br> <strong>QUERY 1 1 1 3 3 3 </strong> <br> <strong>2 4 </strong><br> <strong>UPDATE 2 2 2 1 </strong><br> <strong>QUERY 1 1 1 1 1 1 </strong> <br> <strong>QUERY 1 1 1 2 2 2 </strong> <br> <strong>QUERY 2 2 2 2 2 2 </strong> </pre></p>
                    <p> <strong> Sample Output </strong> <br>
                        <pre> <strong>4</strong><br> <strong>4</strong><br> <strong>27</strong><br> <strong>0</strong><br> <strong>1</strong><br> <strong>1</strong><br></pre> </p>

                    <p> <strong> Explanation </strong> <br>
                        First test case, we are given a cube of 4 * 4 * 4 
                        and 5 queries. Initially all the cells 
                        (1,1,1) to (4,4,4) are 0. <br>

                        UPDATE 2 2 2 4 makes the cell (2,2,2) = 4 <br>
                        QUERY 1 1 1 3 3 3. As (2,2,2) is updated 
                        to 4 and the rest are all 0. 
                        The answer to this query is 4. <br>

                        UPDATE 1 1 1 23. updates the cell (1,1,1) 
                        to 23. QUERY 2 2 2 4 4 4. Only the cell (1,1,1) 
                        and (2,2,2) are non-zero and (1,1,1) 
                        is not between (2,2,2) and (4,4,4). 
                        So, the answer is 4. <br>

                        QUERY 1 1 1 3 3 3. 2 cells are non-zero 
                        and their sum is 23+4 = 27. <br> 

                    </p>

                    <a class="btn btn-link" href="{{ url('/p1/create') }}">Comenzar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection