@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cube Summation</div>
                <div class="panel-body">
                    @foreach ($cubeSummation as $item)
					    {{ $item }}<br />
					@endforeach			    
 				</div>
            </div>
        </div>
    </div>
</div>

@endsection