@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cube Summation</div>
                <div class="panel-body">
                    <div class="form-horizontal" id ="first">                        
                        @include('p1.form.label_input.testcases')
                         
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary" form = "form-result">
                                Resultados
                            </button>
                        </div>
                    </div>
                    @include('p1.form.forms')				    
 				</div>
            </div>
        </div>
    </div>
</div>
<script >
$(document).ready(function(){

    //inputs
    var test_cases = $("#test_cases");

    //forms
    var form_test_cases = $("#form-test-cases");

    //div
    var first = $("#first");


    
    test_cases.change(function(){
        var i;
        var k = $(this).val();   

        $(".test_case_div").remove();

        for(i = 0; i < k ; i++){
            var id = i+1;
            first.append("<div  id = '"+ id+"' class='test_case_div form-group' name= 'test_case_"+ id+"'><div>");            

            $("#"+ id).append("<h5 class = 'col-md-10'>Caso de prueba número "+id+" :</h5>");
        }

        first.children(".test_case_div").each(function(){
            $(this).append("<label for='tamano-matrix' class='col-md-4 control-label'>Tamaño de la matrix  :</label>");
            $(this).append("<div class='col-md-6'><input id='tamano_matrix_"+$(this).attr('id')+"' name='tamano_matrix_"+$(this).attr('id')+"' class='form-control' max='100' min='1' form='form-result' type='number' value='0'></div>");

            $(this).append("<label for='nro_operaciones' class='col-md-4 control-label'>Número de operaciones  :</label>");
            $(this).append("<div class='col-md-6 nro_operaciones'><input id='nro_operaciones' name='nro_operaciones_"+$(this).attr('id')+"' class='form-control ' max='1000' min='1' form='form-result' type='number' value='0'></div>");
        });       

         first.children(".test_case_div").children(".nro_operaciones").each(function(){
            $(this).children(0).change(function(){
                var test_case_raiz = $(this).parent().parent();
                var j;
                var h = $(this).val();

                test_case_raiz.children(".delete-control").each(function(){
                    $(this).remove();
                });

                for(j= 0 ; j < h; j++){
                    var id2 = j+1;
                    test_case_raiz.append("<label class='col-md-4 control-label delete-control'> Operación "+id2+" : </label>");
                    test_case_raiz.append("<div class='col-md-6 operation delete-control' id='"+test_case_raiz.attr('id')+""+id2+"' > <select class='form-control operation-select' name='operation_"+test_case_raiz.attr('id')+""+id2+"' id = '"+test_case_raiz.attr('id')+""+id2+"' form = 'form-result'><option value='Q'>Query</option><option value='U' selected='selected'>Update</option></select></div>");
                }

                first.children(".test_case_div").children(".operation").each(function(){
                    $(this).children(0).change(function(){                        
                        //Validacion para el tamaño maximo de los campos de actualizacion. 
                        var max = $("#tamano_matrix_"+$(this).parent().parent().attr('id')).val();
                        var j;
                        var h = $(this).val();

                        var padre = $(this).parent();

                        var idpadre = $(this).parent().attr('id');

                        padre.children(".query").each(function(){
                            $(this).remove();
                        });
                        padre.children(".update").each(function(){
                            $(this).remove();
                        });

                        if($(this).val() == 'Q'){
                            padre.append("<label class='col-md-4 control-label query' > x1 : </label>");
                            padre.append("<div class='col-md-6 query'> <input id='x1"+idpadre+"' name='x1"+idpadre+"' class='form-control query'  form='form-result' type='number' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label query'> y1 : </label>");
                            padre.append("<div class='col-md-6 query'> <input id='y1"+idpadre+"' name='y1"+idpadre+"' class='form-control query'  form='form-result' type='number' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label query'> z1 : </label> ");
                            padre.append("<div class='col-md-6 query'> <input id='z1"+idpadre+"' name='z1"+idpadre+"' class='form-control query'  form='form-result' type='number' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label query'> x2 : </label>");
                            padre.append("<div class='col-md-6 query'> <input id='x2"+idpadre+"' name='x2"+idpadre+"' class='form-control query'  form='form-result' type='number' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label query'> y2 : </label>");
                            padre.append("<div class='col-md-6 query'> <input id='y2"+idpadre+"' name='y2"+idpadre+"' class='form-control query'  form='form-result' type='number' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label query'> z2 : </label>");
                            padre.append("<div class='col-md-6 query'> <input id='z2"+idpadre+"' name='z2"+idpadre+"' class='form-control query'  form='form-result' type='number' value='0'></div>");


                        }else{
                            padre.append("<label class='col-md-4 control-label update' > x : </label>");
                            padre.append("<div class='col-md-6 update'> <input id='x' name='x"+idpadre+"' class='form-control update'  form='form-result' type='number' max='"+max+"' min='1' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label update'> y : </label>");
                            padre.append("<div class='col-md-6 update'> <input id='y' name='y"+idpadre+"' class='form-control update'  form='form-result' type='number' max='"+max+"' min='1' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label update'> z : </label> ");
                            padre.append("<div class='col-md-6 update'> <input id='z' name='z"+idpadre+"' class='form-control update'  form='form-result' type='number' max='"+max+"' min='1' value='0'></div>");

                            padre.append("<label class='col-md-4 control-label update'> Valor : </label>");
                            padre.append("<div class='col-md-6 update'> <input id='valor' name='valor"+idpadre+"' class='form-control update'  form='form-result' type='number' value='0'></div>");
                        }

                        $("#x1"+idpadre).change(function(){
                            $("#x2"+idpadre).attr("min", $(this).val());
                        });

                        $("#y1"+idpadre).change(function(){
                            $("#y2"+idpadre).attr("min", $(this).val());
                        });

                        $("#z1"+idpadre).change(function(){
                            $("#z2"+idpadre).attr("min", $(this).val());
                        });

                    });
                });

            });
        });

        
             
    });

   
    
});
</script>

@endsection