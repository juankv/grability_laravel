<div class="form-group{{ $errors->has('test_cases') ? ' has-error' : '' }}">
{!!Form::label('test_cases', 'Número de casos de prueba :', 
	[
		'class' => 'col-md-4 control-label',
	]
)!!}

<div class="col-md-6">

{!!Form::number('p1', 0, 
	[	
		'id' => 'test_cases',
		'name' => 'test_cases',
		'class' => 'form-control',
		'placeholder' => 'Ingresa el numero de casos de prueba.',
		'max' 	=>	'50',
		'min'   =>   '1',
		'form'  => 'form-result',
	]
)!!}
</div>
</div>